(function($) {

/* Useful:
addClass() - Adds one or more classes to the selected elements
removeClass() - Removes one or more classes from the selected elements
toggleClass() - Toggles between adding/removing classes from the selected elements
css() - Sets or returns the style attribute
html() - Content of the div
*/

/* Burger Nav */
$(".burger").click(function() {
	$(".burger").toggleClass("active");
	$(".nav.menu").toggleClass("active");
});


/* Code for slick slider

$('.slider-face').slick({
    infinite: true,
	slidesToShow: 2,
	slidesToScroll: 1,
	variableWidth: true,
	dots:true,
	arrows:false,
 });
 */


console.log( 'Welcome to Drew & Rose' );
})( jQuery );
