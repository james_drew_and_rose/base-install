	<footer class="grey-background">
		<div class="wrapper">
			<div class="footer-column"><?php dynamic_sidebar( 'sidebar-1' ); ?></div>
			<div class="footer-column"><?php dynamic_sidebar( 'sidebar-2' ); ?></div>
		</div>
		<p class="copywrite">© <?php echo date("Y"); ?> Website. All rights reserved.</p>
	</footer>
	<?php wp_footer(); ?>
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
	<script type="text/javascript" src="//cdn.jsdelivr.net/npm/slick-carousel@1.8.1/slick/slick.min.js"></script>
	<script type="text/javascript" src="<?php echo get_template_directory_uri(); ?>/scripty.js" type="text/css" /></script>
	</body>
</html>